from sys import argv
from model import *

def main():
	if len(argv) != 2:
		print "Please specify a command"
		return
	opt = argv[1]
	if opt == "emptyTD":
		print "Deleting all TestData documents"
		empty_db_of_TDs()
	elif opt == "emptyTR":
		print "Deleting all TestRun documents"
		empty_db_of_TRs()
	elif opt == "fillTD":
		print "Adding example TestData documents"
		populate_db_with_TDs()
	elif opt == "reset":
		empty_db_of_TDs()
		empty_db_of_TRs()
		populate_db_with_TDs()
		print "Database reset to testable state"
	else:
		print "Invalid command"


if __name__ == "__main__":
	main()