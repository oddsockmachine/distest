# A thin ORM to make it easier to work with the database
from mongoengine import *  # An alternative ORM, which might be more trouble than it's worth
from datetime import datetime, timedelta
from random import randint
from pprint import pprint
import os
import json

connect('distest')

class TR(Document):
	name = StringField(required=True)
	builds = ListField(StringField())
	revision = IntField(required=True)
	timestamp = DateTimeField()
	fullname = StringField(required=True)
	all_tests = ListField(ObjectIdField())
	ready = ListField(ObjectIdField())
	doing = MapField(StringField())  # {TD_id:client}
	passed = ListField(ObjectIdField())  # {TD_id:client}
	failed = ListField(ObjectIdField())  # {TD_id:client}
	state = StringField(required=True)  # started or finished
	progress = IntField()
	def create(self,name,builds,revision,tests):
		self.name = name
		self.builds = builds
		self.revision = revision
		self.all_tests = tests
		self.ready = tests
		self.fullname = self.create_test_run_name(name, revision)
		self.timestamp = datetime.now()
		self.state = "started"
		self.progress = 0
		return
	def create_test_run_name(self, name, revision):
		_date, _time = str(datetime.now()).split(" ")
		_time = _time.replace(":","-").split(".")[0]
		tr_name = "_".join([_date, _time, str(revision), str(name)])
		return tr_name
	def end(self):
		"""
		Move this TR into the finished state - cannot be run from now on.
		"""
		self.state = "finished"
		self.timestamp = datetime.now()
		self.save()
		return
	def start_next_test(self):
		# First check that there are tests remaining
		if len(self.ready) == 0:
			# error - list empty, test run finished
			return  # Returning None will show warning on frontend
		next_test = self.ready.pop()  # Get next from ready queue
		# TODO client will handle runtime, no need to keep track of start time here
		self.doing[str(next_test)] = str(datetime.now())  # Move test to doing state, TODO record client or start time
		self.save()  # update changes to doc
		return next_test  # Return id of this TD
	def end_test(self, td_id, state, runtime):
		"""
		Move a test into either passed or failed state, and update the TD's runtime information
		state must be either 'pass' or 'fail', should have been checked by server
		runtime should be an integer of seconds, recorded and calculated by client
		"""
		# We should have already checked that this TD is in the doing state
		start_time = self.doing.get('td_id')
		del self.doing[td_id]  # Remove this test_id from doing list
		# Check which state to move test to - we have already checked that the new state is valid
		if state == 'pass':
			self.passed.append(td_id)
		elif state == 'fail':
			self.failed.append(td_id)
		# update time on TD
		td = getTD(td_id)
		print "Test took "+str(runtime)
		td.update_time(self.revision, runtime)
		if len(self.ready)==0 and len(self.doing)==0:
			self.state = "finished"
		self.get_progress()  # Only update progress when moving test to done(pass/fail), test in doing doesn't count
		self.save()
		return
	def to_link(self):
		"""
		Create a dict with name and id, to allow this TR to be displayed as a link
		"""
		return {'name':self.name, 'id':self.id}
	def to_client(self):
		dic = {'id':str(self.id),
				'name':self.fullname,
				'revision':self.revision,
				'all_tests_num':len(self.all_tests),
				'progress':self.progress,
				'time_remaining':self.get_time_remaining(),
				'state':self.state,
				'ready':map(str,self.ready),
				'passed':map(str,self.passed),
				'failed':map(str,self.failed),
				}
		return dic
	def to_template(self):
		"""
		Create a dict of properties that can be rendered by the template engine
		"""
		ready_dicts = []
		for i in self.ready:
			ready_dicts.append({'id':[i],
								'td':getTD(i).to_template()})
		doing_dicts = []
		for i in self.doing:
			doing_dicts.append({'id':[i],
								'td':getTD(i).to_template()})
		passed_dicts = []
		for i in self.passed:
			passed_dicts.append({'id':[i],
								'td':getTD(i).to_template()})
		failed_dicts = []
		for i in self.failed:
			failed_dicts.append({'id':[i],
								'td':getTD(i).to_template()})
		template = {'id':str(self.id),  # TODO - check this is ok, nobody needs an OId object
					'name':self.fullname,
					'revision':self.revision,
					'all_tests_num':len(self.all_tests),
					'ready_num':len(self.ready),
					'passed_num':len(self.passed),
					'doing_num':len(self.doing),
					'failed_num':len(self.failed),
					'readypc':int(len(self.ready)/len(self.all_tests))*100,  # Ready as percentage
					'doingpc':int(len(self.doing)/len(self.all_tests))*100,
					'passpc':int(len(self.passed)/len(self.all_tests))*100,
					'failpc':int(len(self.failed)/len(self.all_tests))*100,
					'timestamp':self.timestamp,
					'testsReady':ready_dicts,
					'testsDoing':doing_dicts,
					'testsPassed':passed_dicts,
					'testsFailed':failed_dicts,
					'progress':self.progress,
					'time_remaining':self.get_time_remaining(),
					'state':self.state}
		return template
	def output(self):
		out = " ~ ".join([ "Name: " + self.fullname,
		 "Revision: " + str(self.revision),
		 "Builds: " + str(self.builds),
		 "Total tests: " + str(self.all_tests),])
		print out
		return out
	def get_progress(self):
		"""
		Calculate the progress in percent of this TR, save and return
		"""
		progress = int(100* ((len(self.passed)+len(self.failed))/float(len(self.all_tests))))
		self.progress = progress
		self.save()
		return progress
	def get_time_remaining(self):
		int_remaining = 0  # time remaining in seconds
		for td in self.ready:  # add up all remaining tests
			int_remaining += int(getTD(td).expd_time)
		remaining = str(timedelta(seconds=int_remaining))
		return remaining
	
class TD(Document):
	name = StringField(required=True)  # = TS+/+TC+/+build
	test_suite = StringField(required=True)
	test_case = StringField(required=True)
	build_var = StringField(required=True)
	path = StringField(required=True)  # Relative path to test from sandbox root
	command = StringField(required=True)  # Command to start test
	expd_time = IntField()  # (Rolling?) Average of runtimes 
	time_history = MapField(IntField())  # Map int(revision):runtime
	def create(self, TS, TC, build, path, command):
		self.test_suite = TS  # TS_*
		self.name = TS+"/"+TC+"/"+build  # of the form TS_*/TC_*/build
		self.test_case = TC  #TC_*
		self.build_var = build  # eg: 40_debug
		self.path = path
		self.command = command
		expd_time = 0
		time_history = []
		return self
	def run(self):
		# Rely on client to provide timing info
		result = ""
		return result
	def update_time(self, revision, new_time):
		"""
		Store the latest runtime.
		all times can be looked up by revision
		When displaying, invert dictionary, so each runtime has one or more revisions
		calculate average of all times - this is expected time to run this test.
		TODO - only average over last (x) revisions.
		"""
		# Rely on client to provide timing info
		#pprint (self.time_history)
		#print "revision:"+str(revision)
		#print "new time:"+str(new_time)
		# associate runtime with this revision. Multiple runs on same revision will overwrite, but doesn't matter.
		self.time_history[str(revision)] = (int(new_time))
		time_sum = sum(map(int, self.time_history.itervalues()))
		num = len(self.time_history)
		#print "sum:"+str(time_sum)
		#print "total:"+str(num)
		self.expd_time = int(time_sum/num)  #int( sum(map(int, self.time_history.itervalues()))/len(self.time_history) )  # explicitly return int
		self.save()
		return
	def to_json(self):
		# Return json representation of TD, to send to client.
		return json.dumps({
				'id':str(self.id),
				'name':self.name,
				'test_suite':self.test_suite,
				'test_case':self.test_case,
				'build_var':self.build_var,
				'path':self.path,
				'command':self.command,
				})
	def to_template(self):
		#pprint (self.expd_time)
		#pprint (self.time_history)
		return {'name':self.name,
				'id':str(self.id),
				'test_suite':self.test_suite,
				'test_case':self.test_case,
				'build_var':self.build_var,
				'expd_time':self.expd_time,
				'time_history':self.time_history.itervalues(),
				}
	def to_client(self):
		dic = {'name':self.name,
				'id':str(self.id),
				'test_suite':self.test_suite,
				'test_case':self.test_case,
				'build_var':self.build_var,
				}
		return dic


def populate_db_with_TDs():
	"""
	For testing purposes, fill the database with example TDs.
	A similar function could be used in production. 
	"""
	with open("test_names.txt", 'r') as open_file:
		lines = open_file.readlines()
		for line in lines:
			fullname = line.split(", ")[0]
			TC = fullname.split("/")[~0]
			TS = fullname.split("/")[0]
			command = "make VARIANT=0 TGT=OPTIMAL test"
			for build in ["40_debug","3X_debug","40_release","3X_release"]:
				td = TD()
				td.create(TS, TC, build, "./Tests/"+fullname, command )
				td.update_time(0, randint(1000))
				td.save()
	return

def empty_db_of_TDs():
	"""
	Clear the database of all TDs. Only for testing purposes.
	"""
	for td in TD.objects():
		td.delete()


def empty_db_of_TRs():
	"""
	Clear the database of all TRs. Only for testing purposes.
	"""
	for tr in TR.objects():
		tr.delete()

def getTD(oid):
	"""
	Shorthand to get a TD by id
	"""
	ret = TD.objects(id=oid)
	if len(ret)>0:
		return ret[0]
	else:
		return None

def getTR(oid):
	"""
	Shorthand to get a TR by id
	"""
	ret = TR.objects(id=oid)
	if len(ret)>0:
		return ret[0]
	else:
		return None

def getTR_fn(tr_fn):
	"""
	Get TR by fullname
	"""
	ret = TR.objects(fullname=tr_fn)
	if len(ret)>0:
		return ret[0]
	else:
		return None