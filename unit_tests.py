# Unit Tests

import unittest
from distest import *  # Bad, but simple



class TestDistTester(unittest.TestCase):
	def setup(self):
		return
	def test_get_tests(self):
		results = get_tests_in_order("test_names.txt")
		# Check total
		self.assertEqual(len(results), 26)
		# check Quickest
		self.assertEqual(results[0], 'TS_D/TC_Tash')
		# Check longest
		self.assertEqual(results[~0], 'TS_E/TC_Yolande')
		return
	def test_filter_by_TS(self):
		# Don't filter
		results = (filter_by_ts(get_tests_in_order("test_names.txt"), []))
		self.assertEqual(len(results), 26)
		self.assertEqual(results[0], 'TS_D/TC_Tash')
		self.assertEqual(results[~0], 'TS_E/TC_Yolande')
		# Only TS_A
		results = (filter_by_ts(get_tests_in_order("test_names.txt"), ["TS_A"]))
		self.assertEqual(len(results), 5)
		self.assertEqual(results[0], 'TS_A/TC_Carly')
		self.assertEqual(results[~0], 'TS_A/TC_Dave')
		# TS_B and TS_C
		results = (filter_by_ts(get_tests_in_order("test_names.txt"), ["TS_B", "TS_C"]))
		self.assertEqual(len(results), 9)
		self.assertEqual(results[0], 'TS_C/TC_Neil')
		self.assertEqual(results[~0], 'TS_B/TC_Kass')
		# All
		results = (filter_by_ts(get_tests_in_order("test_names.txt"), ["TS_A","TS_B","TS_C","TS_D","TS_E","TS_F"]))
		self.assertEqual(len(results), 26)
		self.assertEqual(results[0], 'TS_D/TC_Tash')
		self.assertEqual(results[~0], 'TS_E/TC_Yolande')
		return

	def test_cl_args(self):
		return

	def test_cfg_file(self):
		tests = get_tests_from_config("./my_tests.cfg")
		self.assertEqual(len(tests), 2)  # 2 TS's in test cfg file
		self.assertFalse("bs_c" in tests)  # invalid TS rejected
		self.assertTrue("TS_B" in tests)  # valid TS extracted
		return

	def test_get_required(self):
		tests = get_required_tests(get_testdata_db(), "./my_tests.cfg", None)
		self.assertEqual(len(tests), 14)  # 14 tests
		
	def test_order(self):
		db = get_testdata_db()
		tests = get_required_tests(db, "./my_tests.cfg", None)
		ordered = order_tests(db, tests)
		self.assertGreater(t_(db, ordered[0]).get('expd_time'), t_(db, ordered[-1]).get('expd_time'))
		return

	def test_update_finished(self):
		
		return

	def test_integration(self):
		# Get database connections
		tddb = get_testdata_db()
		trdb = get_testrun_db()
		# Clear the databases for testing
		empty_db(tddb)
		empty_db(trdb)
		populate_examples(tddb)
		# Find the tests we want to run
		requested_test_ids = get_required_tests(tddb, "./my_tests.cfg", None)
		# Get them in order
		test_list_1 = order_tests(tddb, requested_test_ids)
		# Put them into the test_list db, get test_list id back
		tl_id_1 = put_test_list_on_db(trdb, test_list_1)
		# Retrieve the same test list from db using id
		test_list_2 = get_test_list_from_db(trdb, tl_id_1)

		# Check the ids and documents(contents) match
		self.assertEqual(tl_id_1, test_list_2.get('_id'))
		self.assertEqual(test_list_1, test_list_2.get('ready'))

	def test_start_test(self):

		tddb = get_testdata_db()
		trdb = get_testrun_db()
		tl_id = setup_tests()
		start_tl = trdb.find_one({'revision':'12345'})
		print "STL: " +str(start_tl)
		test_id = start_tl.get('ready')[0]
		output = start_test(tddb, trdb, 12345, test_id)
		end_tl = trdb.find_one({'revision':'12345'})
		print "STL: " +str(end_tl)
		
		return
if __name__ == '__main__':
	unittest.main()
