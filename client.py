from flask import Flask, render_template, request, redirect, url_for, json
from pprint import pformat
from distest import *
from os import system
from sys import exit
from futures import *
import model as m
import json
import requests
import client_config as config

app = Flask(__name__)
#joinedTRs = config.joinedTRs


def fetchTR(tr_id):
	"""
	# Get a TR dict from server's db
	"""
	tr = json.loads(fetch("getTRs/?tr_id="+tr_id))
	return tr

def fetchTD(tr_id):
	"""
	# Get a TD dict from server's db
	"""
	td = json.loads(fetch("getTDs/?td_id="+td_id))
	return td

def fetch(url):
	ip = config.get('server_ip')
	port = config.get('server_port')
	if not ip or not port:
		# Ensure ip and port are specified
		return None
	url = "http://"+ip+":"+str(port)+"/api/"+url

	try:
		# Get contents from server, return
		r = requests.get(url)
		return r.text
	except:
		# Problem with connection
		return None

def test_connection(ip):
	"""
	Try to connect to the server
	return true if connection is successful, none if not
	"""
	return fetch("connect")





@app.route("/")
def index():
	"""
	Dashboard
	Show what TRs currently running on this client
	Time remaining, uptime etc
	Links to show all TRs (to join, view data), TDs (on server)
	"""
	joined = [fetchTR(j) for j in config.joinedTRs]
	return render_template("c_index.html", joined=joined)


@app.route("/TR/")
def getTR():
	"""
	No args: Show all TRs that the central server knows about
	tr_id: show details for this TR, with option to join
	"""
	tr_json = None

	tr_id = request.args.get('tr_id')
	if tr_id:
		tr_json = fetch("getTRs/?tr_id="+tr_id)  # so get from server
	if tr_json != None:  # ie we have retrieved a valid tr
		print "got tr"
		tr = json.loads(tr_json)
		# Construct a weblink that will point to this TR on the server
		tr['link'] = "http://"+config.get('server_ip')+":"+str(config.get('server_port'))+"/TR/?tr_id="+tr_id
		print tr['link']
		return render_template("c_one_tr.html", tr=tr)
	else:  # Couldn't get a valid tr, show all to select
		print "no tr, show all"
		trs_json = fetch("getTRs")
		trs = json.loads(trs_json)
		return render_template('c_all_trs.html', trs=trs)



@app.route("/join/")
def joinTR():
	"""
	No args: Return error
	tr_id: subscribe this client to the TR
	"""
	tr_id = request.args.get('tr_id')
	if not tr_id:
		return "No TR specified"
	# Join this TR
	# Add tr_id to persistent storage - incase of crash/shutdown
	# Checkout code from this TR's revision
	# Add list of tests to worker queue
	config.joinedTRs.append(tr_id)
	get_next_test_from_TR(tr_id)
	return redirect(url_for('running', tr_id=tr_id))




@app.route("/running/")
def running():
	"""
	No args: Show all TRs this client has joined
	tr_id: show data about this TR, if client has joined it
	"""
	tr_id = request.args.get('tr_id')
	if not tr_id:
		return str(config.joinedTRs)
	if not config.is_joined(tr_id):
		return "You have not yet joined this TR"
	tr = fetchTR(tr_id)
	print tr
	tr['doing'] = ["Tests running", "on this client", "go here!"]
	return render_template('c_one_running.html', tr=tr)


def get_next_test_from_TR(tr_id):
	"""
	Ask the server for details on the next test that should be run.
	Validate that this client is subscribed to this TR first.
	Record the time this test was started - the runtime must be sent to the server.
	"""
	if not config.is_joined(tr_id):
		# Error - have not joined this TR yet.
		return None
	response = fetch('pull_test/?tr_id='+tr_id)
	print "****************"
	print response
	print "****************"
	return response

def run_a_test():
	"""
	Launch a test on the client's machine
	"""
#	result = system('start runtest.bat dir/to/test')
#	result = get_result_from_log()
#	return result
	return

if __name__ == "__main__":
	server_ip = config.get('server_ip')
	server_port = config.get('server_port')
	print "Starting client process"
	print "Connecting to central server at "+server_ip+":"+server_port+"..."
	if not test_connection(server_ip):
		print "Couldn't connect to server"
		exit()
	print "Connected!"
	port = int(config.get("client_port"))
	app.run(debug=True, port=port)
