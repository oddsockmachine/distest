# Module for storing global/persistent data

import json

joinedTRs = []  # globally accessible list of which TRs this client has joined 
config = {}  # globally accessible dict of cached config parameters

def get_joined_TRs_from_file():
	"""
	Check the client_joined_TRs.txt file for any TRs that were joined before the client
	last closed.
	"""
	global joinedTRs
	with open('client_joined_TRs.txt', 'r') as TR_file:
		joinedTRs = [t for t in TR_file.readlines() if len(t)>1]  # len qualifier excludes "" and "\n" entries
	return joinedTRs

def is_joined(tr_id):
	if tr_id in joinedTRs:
		return True
	else:
		return False

def save_joined_TRs_to_file():
	"""
	Update the client_joined_TRs.txt file whenever we join a new TR.
	"""
	return

def get(key):
	"""
	Open the client's config file, and return the specified element
	"""
	global config
	if len(config)==0:
		# getting conf from file
		with open('client_config.json', 'r') as conf_file:
			config = json.load(conf_file)
	# getting conf from cache
	return config.get(key)

get_joined_TRs_from_file()