import unittest
import client_config as config
from timeit import timeit

class TestConfigModule(unittest.TestCase):

	def setUp(self):
		self.jtrs = config.get_joined_TRs_from_file()
		return

	def test_joined_TRs(self):
		self.assertEqual(len(self.jtrs), 1)

	def test_is_joined(self):
		self.assertTrue(config.is_joined('52bda91959aae6104091d20b'))
		self.assertFalse(config.is_joined('52bda91959aae6104091d20z'))

	def test_config(self):
		# Check that subsequent reads of the config dict are from the cache, not the file
		from_file = timeit("config.get('server_ip')", setup="import client_config as config", number=1)
		from_cache = timeit("config.get('server_ip')", setup="import client_config as config", number=1)
		self.assertGreater(from_file, from_cache)

		# Check the expected values
		self.assertEqual(config.get('server_ip'), "127.0.0.1")
		self.assertEqual(config.get('server_port'), "5000")



if __name__ == '__main__':
    unittest.main()